package ro.stiam.stiri;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.media.ThumbnailUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;


public class ArticleListingAdapter extends ArrayAdapter<JSONObject> {
  private final Context context;

  public ArticleListingAdapter(Context context, List<JSONObject> values) {
    super(context, R.layout.activity_article_item, values);
    this.context = context;
  }

  @Override
  public View getView(int position, View convertView, ViewGroup parent) {
    LayoutInflater inflater = (LayoutInflater) context
        .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    View rowView = inflater.inflate(R.layout.activity_article_item, parent, false);
    TextView titleView = (TextView) rowView.findViewById(R.id.article_title);
    TextView descriptionView = (TextView) rowView.findViewById(R.id.article_description);
    final ImageView imageView = (ImageView) rowView.findViewById(R.id.article_image);
    JSONObject article = getItem(position);
    try {
    	titleView.setText(article.getString("title"));
	} catch (JSONException e) {
		titleView.setText("JSON ERROR!");
	}
    try {
    	descriptionView.setText(article.getString("description"));
	} catch (JSONException e) {
		descriptionView.setText("JSON ERROR!");
	}
    
    if (imageView.getDrawable() == null){
    
	    try {
	    	URL thumb_u = new URL(article.getString("thumbnail"));
	
	    	HttpURLConnection connection = (HttpURLConnection) thumb_u.openConnection();
	    	connection.connect();
	    	InputStream input = connection.getInputStream();
	    	
	
	    	final Bitmap image = BitmapFactory.decodeStream(input);
	
	    	Drawable thumb_d = new BitmapDrawable(context.getResources(), image);
	    	imageView.setImageDrawable(thumb_d);

	    	imageView.measure(MeasureSpec.makeMeasureSpec(0, MeasureSpec.UNSPECIFIED),
	    			MeasureSpec.makeMeasureSpec(0, MeasureSpec.UNSPECIFIED));
	    	int targetWidth = imageView.getMeasuredWidth() * 2; //TODO: bad, need to change
	    	
			final int imageHeight = image.getHeight();
			final int imageWidth = image.getWidth();
			
			imageView.setLayoutParams(new LinearLayout.LayoutParams(
					LinearLayout.LayoutParams.MATCH_PARENT,
					LinearLayout.LayoutParams.MATCH_PARENT));
			
			float scale = (float) targetWidth / imageWidth;
			int newHeight = (int) Math.round(imageHeight * scale);
			imageView.getLayoutParams().height = newHeight;
	
	    }
	    catch (Exception e) {
	    	imageView.setVisibility(View.GONE);
	    }
    }
    
    return rowView;
  }

  
}