package ro.stiam.stiri;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


//import android.app.Activity;
import android.app.ListActivity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.Window;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.ListView;


public class MainActivity extends ListActivity {
	private String url = "http://stiam.ro/revista-presei-romanesti/query.json?_=1384044530542";
	private int nextPageStart = 0;
	private final List<JSONObject> list = new ArrayList<JSONObject>();
	private boolean loadingMore = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_main);

        View footerView = ((LayoutInflater)this.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.list_footer, null, false);
        this.getListView().addFooterView(footerView);
        
        StiamApp application = (StiamApp) getApplication();
        
        JSONObject json = application.getJSON(url);
        updateList(json);
		ArticleListingAdapter adapter = new ArticleListingAdapter(this, this.list);
        setListAdapter(adapter);
        
        OnScrollListener listener = getOnScrollListener();
        this.getListView().setOnScrollListener(listener);
    }

    public void updateList(JSONObject json){
        JSONArray items;
		try {
			items = json.getJSONArray("items");
			for(int i = 0; i < items.length(); i++){
				JSONObject object = items.getJSONObject(i); 
				this.list.add(object);
			}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
    
    private Runnable loadMoreListItems = new Runnable() {
    	@Override
    	public void run() {
    		loadingMore = true;
			StiamApp application = (StiamApp) getApplication();
			String pageUrl = url + "&b_start=" + nextPageStart;
			Log.i("pageUrl", pageUrl);
			JSONObject json = application.getJSON(pageUrl);
			updateList(json);
       		runOnUiThread(returnRes);
    	}
    };
    
    
    private Runnable returnRes = new Runnable() {
    	@Override
    	public void run() {
    		((ArticleListingAdapter) getListAdapter()).notifyDataSetChanged();
    		loadingMore = false;
       };
    };
    
    
    public OnScrollListener getOnScrollListener(){
    	return new OnScrollListener(){
    		@Override
    		public void onScrollStateChanged(AbsListView view, int scrollState) {
    			
    		}
    		
    		@Override
    		public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
    			int lastInScreen = firstVisibleItem + visibleItemCount;
    			if((lastInScreen == totalItemCount) && !(loadingMore)){
    				nextPageStart = list.size();
    				Thread thread =  new Thread(null, loadMoreListItems);
    				thread.start();
    			}
    		}
    	};
    }
    
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }
    
    @Override
    public void onListItemClick(ListView l, View v , int position, long id){
  	  super.onListItemClick(l, v, position, id);
  	  
  	  JSONObject item = (JSONObject) l.getItemAtPosition(position);
  	  
  	  Bundle b = new Bundle();
  	  b.putString("articleJSON", item.toString());
  	  Intent i = new Intent(MainActivity.this, ArticleView.class);
  	  i.putExtras(b);
  	  startActivity(i);
    }
    
    public JSONObject getArticleAtPosition(int position){
    	return this.list.get(position);
    }
       
}