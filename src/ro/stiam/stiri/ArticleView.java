package ro.stiam.stiri;

import java.io.InputStream;
import java.lang.ref.WeakReference;
import java.net.HttpURLConnection;
import java.net.URL;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.Menu;
import android.view.View;
import android.view.Window;
import android.view.View.MeasureSpec;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class ArticleView extends Activity {
	private String bodyText = new String();
	private JSONObject article = new JSONObject();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_article_view);

		Bundle b = getIntent().getExtras();
		try {
			article = new JSONObject(b.getString("articleJSON"));
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		final HandlerClass mHandler = new HandlerClass(this);

        // Start lengthy operation in a background thread
        new Thread(new Runnable() {
            public void run() {
            	try {
					bodyText = getArticleBody(article.getString("url"), article.getString("original")).getString("text");
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
            	mHandler.sendEmptyMessage(0);
            }
            
        }).start();
        
        loadContent(article);
        
	}
	
	
	public void loadBody(){
		ProgressBar mProgress = (ProgressBar) findViewById(R.id.progress_bar);
		TextView bodyView = (TextView) this.findViewById(R.id.article_body);
		bodyView.setText(bodyText);
		mProgress.setVisibility(View.GONE);
	}
	
	
	public void loadContent(JSONObject article){
		ImageView imageView = (ImageView) this.findViewById(R.id.article_image);
		TextView titleView = (TextView) this.findViewById(R.id.article_title);
		TextView descriptionView = (TextView) this.findViewById(R.id.article_description);
		TextView bodyView = (TextView) this.findViewById(R.id.article_body);
		bodyView.setText(bodyText);
		
		try {
			titleView.setText(article.getString("title"));
			descriptionView.setText(article.getString("description"));
			
		    try {
		    	URL thumb_u = new URL(article.getString("thumbnail"));
		    	
		    	HttpURLConnection connection = (HttpURLConnection) thumb_u.openConnection();
		    	connection.connect();
		    	InputStream input = connection.getInputStream();
		    	
		    	imageView.measure(MeasureSpec.makeMeasureSpec(0, MeasureSpec.UNSPECIFIED),
		    			MeasureSpec.makeMeasureSpec(0, MeasureSpec.UNSPECIFIED));
		    	int targetWidth = imageView.getMeasuredWidth() * 7; //TODO: bad, need to change
		    	
		    	final Bitmap image = BitmapFactory.decodeStream(input);
		    	
		    	Drawable thumb_d = new BitmapDrawable(getResources(), image);
		    	imageView.setImageDrawable(thumb_d);

		    	final int imageHeight = image.getHeight();
				final int imageWidth = image.getWidth();
				
				imageView.setLayoutParams(new RelativeLayout.LayoutParams(
						RelativeLayout.LayoutParams.MATCH_PARENT,
						RelativeLayout.LayoutParams.MATCH_PARENT));
				
				float scale = (float) targetWidth / imageWidth;
				int newHeight = (int) Math.round(imageHeight * scale);
				imageView.getLayoutParams().height = newHeight;
		    }
		    catch (Exception e) {
		    	imageView.setVisibility(View.GONE);
		    }
		} catch (JSONException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	}

	public JSONObject getArticleBody(String url, String original){
		StiamApp application = (StiamApp) getApplication();
		String[] splitUrl = url.split("/");
		String baseUrl = new String();
		for(int i = 0; i < splitUrl.length - 1; i++){
			String delimiter = new String();
			if (i != splitUrl.length -2){
				delimiter = "/";
			} else {
				delimiter = "";
			}
			baseUrl = baseUrl.concat(splitUrl[i] + delimiter);
		}
		String finalUrl = baseUrl.concat("/diffbot.json?url=");
		finalUrl = finalUrl.concat(original);
		return application.getJSON(finalUrl);
	}

	
	private static class HandlerClass extends Handler{
		private final WeakReference<ArticleView> mTarget; 
		public HandlerClass(ArticleView context) {
			mTarget = new WeakReference<ArticleView>((ArticleView) context);
		}
		@Override
		public void handleMessage(Message msg) {
			ArticleView target = mTarget.get();
			if (target != null){
				target.loadBody();
			}
		}
	}
	
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.article_view, menu);
		return true;
	}

}
